# API REST de consulta de datos de OpenStreetMap

Esta API REST permite consultar y obtener datos formateados y limpios de OpenStreetMap a través de una interfaz RESTful. Los datos se devuelven en formato JSON y están libres de nulos.

***
## Este es un repositorio en Construcción
***

## Instalación

1. Clona este repositorio en tu equipo
2. Instala las dependencias necesarias utilizando npm o yarn: `npm install` o `yarn install`
3. Inicia el servidor de la API utilizando npm o yarn: `npm start` o `yarn start`

## Uso

La API cuenta con los siguientes endpoints:

- `GET /data`: Devuelve los datos de OpenStreetMap formateados y limpios en formato JSON
- `GET /data/:id`: Devuelve los datos de un elemento específico de OpenStreetMap a partir de su ID

## Licencia

Este proyecto está licenciado bajo la licencia MIT. Puedes utilizar, modificar y distribuir este proyecto siempre y cuando incluyas una copia de la licencia y reconozcas al autor original.

## Créditos

Este proyecto usa el API Pública del proyecto [https://www.openstreetmap.org] 
Parte de este proyecto fue creado con [https://chat.openai.com]
