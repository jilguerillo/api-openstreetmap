import request from "request";
import express from "express";
const app = express();

const config = {
  // baseUrl: "https://jsonplaceholder.typicode.com/users",
  baseUrl: "https://nominatim.openstreetmap.org/",
  format: "json",
  // q:"santiago",
  // key: <YOUR API KEY OSM>
};

const getDataFromOpenStreetMap = (query) => {
  return new Promise((resolve, reject) => {
    const options = {
      url: `${config.baseUrl}search?format=${config.format}&q=${query}=1&key=${config.key}`,
      //url: `${config.baseUrl}`,
      json: true,
    };

    request(options, (error, response, body) => {
      if (error) {
        console.error("-->", error);
        reject(error);
      } else {
        if (response.statusCode === 404) {
          console.error("Resource not found");
          console.log("--> body ", body);
          console.log("--> response ", response);

          reject(new Error("Resource not found"));
        } else {
          const data = Array.isArray(body) ? body : [body];

          data.map((item) => {
            return {
              name: item.display_name,
              latitude: item.lat,
              longitude: item.lon,
            };
          });

          resolve(data);
        }
      }
    });
  });
};

const getData = async (req, res) => {
  try {
    const data = await getDataFromOpenStreetMap(req.query.q);
    res.json({ data });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

app.get("/data", getData);

app.listen(3000, () => {
  console.log("API listening on port 3000");
});
